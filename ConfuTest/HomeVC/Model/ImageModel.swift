//
//  ImageModel.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

struct ImageModel {
    
    var url: URL
    var uuid: String
    var image: UIImage?
    var dowloadProgress: Float?
}
