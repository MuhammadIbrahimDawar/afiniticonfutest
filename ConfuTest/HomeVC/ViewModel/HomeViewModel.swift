//
//  HomeViewModel.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

protocol HomeViewModelDelegate {
    func reloadData()
    func refreshProgressBar(at index: Int)
}

class HomeViewModel
{
    private var networkManager: NetworkManager
    var delegate: HomeViewModelDelegate?
    private(set) var images = [ImageModel]()
    
    var areAllImagesDownloaded: Bool {
        get{
            var allDownloaded = true
            for image in images{
                if image.image == nil {
                    allDownloaded = false
                }
            }
            return allDownloaded
        }
    }
    
    init() {
        networkManager = NetworkManager()
        networkManager.delegate = self
    }
    
    func startDownloadingImages(imagesUrl: [String])
    {
        initialiseImages(with: imagesUrl)
        
        networkManager.noOfThreadRequired = images.count
        
        var threadNo = images.count
        
        for image in images {
            networkManager.downloadData(from: image.url, with: image.uuid, onthread: threadNo)
                threadNo -= 1
        }
       
    }
    
    private func initialiseImages(with urls: [String])
    {
        for urlString in urls
        {
            if let url = URL(string: urlString)
            {
                images.append(ImageModel(url: url, uuid: UUID().uuidString + urlString, image: nil))
            }
        }
    }
    
   
    
}

extension HomeViewModel: NetworkManagerDelegate
{
    func networkManager(_ networkManager: NetworkManager, didGet data: Data, with uuid:String) {
        if let image = UIImage(data: data)
        {
            if let index = images.firstIndex(where: { (imageModel) -> Bool in
                if imageModel.uuid == uuid {
                    return true
                }
                return false
            })
            {
                
                DispatchQueue.main.async { [unowned self] in
                    self.images[index].image = image
                    self.delegate?.reloadData()
                }
                
            }
            
        }
        
    }
    
    func networkManager(_ networkManager: NetworkManager, didFailWith Error: Error) {
        
    }
    
    func networkManager(_ networkManager: NetworkManager, didGet downloadProgress: Float, for uuid: String) {
        
        if let index = images.firstIndex(where: { (imageModel) -> Bool in
            if imageModel.uuid == uuid {
                return true
            }
            return false
        }){
            DispatchQueue.main.async { [unowned self] in
                self.images[index].dowloadProgress = downloadProgress
                self.delegate?.refreshProgressBar(at: index)
            }
            
        }
        
    }
    
    
}
