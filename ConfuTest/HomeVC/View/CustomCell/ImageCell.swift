//
//  ImageCell.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var customBackgroundView: UIView!
    
    @IBOutlet weak var customImageView: UIImageView!
    
    @IBOutlet weak var imageDownloadProgressBar: UIProgressView!{
        didSet{
            imageDownloadProgressBar.progress = 0.0
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customBackgroundView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
