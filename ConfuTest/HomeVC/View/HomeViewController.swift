//
//  HomeViewController.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

fileprivate enum ActionButtonState {
    case startDownloading
    case sendImages
}

class HomeViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var actionButton: UIButton!
    
    let imagesUrls = ["https://eoimages.gsfc.nasa.gov/images/imagerecords/73000/73751/world.topo.bathy.200407.3x5400x2700.jpg"
                      ,"https://images.unsplash.com/photo-1541531455306-a1008c85edd9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&auto=format&fit=crop&w=900&q=60"
                      ,"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Snake_River_%285mb%29.jpg/1600px-Snake_River_%285mb%29.jpg"]
    
    private var viewModel: HomeViewModel!
    private var actionButtonState: ActionButtonState = .startDownloading
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupTableView()
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupActionButton()
    }
    
    private func setupActionButton()
    {
        switch actionButtonState{
        case .startDownloading:
            actionButton.setTitle("Start Downloading", for: .normal)
            actionButton.isEnabled = true
            break
        case .sendImages:
            actionButton.setTitle("Send Images", for: .normal)
            actionButton.isEnabled = true
            break
        }
    }
    
    private func setupTableView()
    {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: NibIdentifier.kImageCell, bundle: nil), forCellReuseIdentifier: CellIdentifier.kImageCell)
    }
    
    private func setupViewModel()
    {
        viewModel = HomeViewModel()
        viewModel.delegate = self
    }
    
    @IBAction func actionButtonTapped(_ sender: UIButton)
    {
        actionButton.isEnabled = false
        switch actionButtonState {
        case .startDownloading:
            viewModel.startDownloadingImages(imagesUrl: imagesUrls)
            break
        case .sendImages:
            let connectionViewController = ConnectionViewController(nibName: NibIdentifier.kConnectionVC, bundle: nil)
            connectionViewController.images = viewModel.images
            self.navigationController?.pushViewController(connectionViewController, animated: true)
            break
      
        }
       
    }
}

// MARK:- TableView Delegate And Datasource


extension HomeViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        viewModel.images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.kImageCell, for: indexPath) as? ImageCell
        else{
            return UITableViewCell()
        }
        
        if let image = viewModel.images[indexPath.row].image {
            cell.customImageView.image = image
            
            if let downloadProgress = viewModel.images[indexPath.row].dowloadProgress
            {
                cell.imageDownloadProgressBar.progress = downloadProgress
            }
        }
        
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        250
    }
    
}

// MARK:- HomeViewDelegate
extension HomeViewController: HomeViewModelDelegate
{
    func reloadData() {
        
        if viewModel.areAllImagesDownloaded {
            actionButtonState = .sendImages
            setupActionButton()
        }
    
        tableView.reloadData()
        
    }
    
    func refreshProgressBar(at index: Int) {
        
        if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ImageCell
        {
            tableView.beginUpdates()
            cell.imageDownloadProgressBar.progress = viewModel.images[index].dowloadProgress ?? 0.0
            tableView.endUpdates()
        }

       
    }
    
    
}
