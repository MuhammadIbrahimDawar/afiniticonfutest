//
//  UIView+Extension.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

extension UIView
{
    // method to add shadows with provided configuration
    func addShadow(opacity: Float, radius: CGFloat, color: UIColor)
    {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
    // method to round corners with privided corners
    func roundCorners(with radius: CGFloat)
    {
        self.layer.cornerRadius = radius
    }
    
   
    // method to add drop down shadow with predefined configuration
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}
