//
//  Constants.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import Foundation

struct NibIdentifier {
    static let kHomeVC = "HomeViewController"
    static let kImageCell = "ImageCell"
    static let kConnectionVC = "ConnectionViewController"
}

struct CellIdentifier {
    static let kImageCell = "ImageCell"
}
