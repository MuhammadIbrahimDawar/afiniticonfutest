//
//  ConnectionViewModel.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

protocol ConnectionViewModelDelegate:class {
    func showActivityIndicator()
    func hideActivityIndicator()
    func showAlert(with message: String)
    func updateConnectionStatus(with status: Bool)
    func didGetNewImage()
}

class ConnectionViewModel
{
    var delegate: ConnectionViewModelDelegate?
    
    private var socketManager: SocketDataManager
    
    private(set) var receivedImages = [UIImage]()
    
    init() {
        socketManager = SocketDataManager()
        socketManager.delegate = self
    }
    
    func connect(to ipAddress: String, on portNo: Int)
    {
        let connectionModel = ConnectionModel(ipAddress: ipAddress, port: portNo)
        socketManager.connectWith(socket: connectionModel)
    }
    
    func send(images: [ImageModel])
    {
        delegate?.showActivityIndicator()
        var imagesData = [Data]()
        for image in images{
            if let imageData = image.image?.jpegData(compressionQuality: 0.5)
            {
                imagesData.append(imageData)
            }
        }
        socketManager.data = imagesData
        socketManager.send()
    }
    
    func closeConnection()
    {
        socketManager.disconnect()
    }
}

extension ConnectionViewModel: SocketDataManagerDelegate
{
    
    func updateConnection(with status: Bool)
    {
        delegate?.updateConnectionStatus(with: status)
    }
    
    func dataSent() {
        delegate?.hideActivityIndicator()
    }
    
    func didGet(data: Data) {
        if let image = UIImage(data: data)
        {
            receivedImages.append(image)
            DispatchQueue.main.async { [weak self] in
                self?.delegate?.didGetNewImage()
            }
        }
    }
}
