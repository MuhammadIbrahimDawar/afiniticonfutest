//
//  ConnectionModel.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import Foundation

struct ConnectionModel {
    
    let ipAddress: String
    let port: Int

}
