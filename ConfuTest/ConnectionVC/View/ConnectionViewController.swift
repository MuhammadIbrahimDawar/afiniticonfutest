//
//  ConnectionViewController.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

fileprivate enum ActionButtonState {
    case connection
    case send
}

class ConnectionViewController: UIViewController {
    
    @IBOutlet private weak var connectionStatusLabel: UILabel!
    
    @IBOutlet private weak var ipAddressTextField: UITextField!
    
    @IBOutlet private weak var portNoTextField: UITextField!
    
    @IBOutlet private weak var actionButton: UIButton!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!{
        didSet{
            activityIndicator.isHidden = true
        }
    }
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var viewModel: ConnectionViewModel!
    private var actionButtonState: ActionButtonState = .connection
    
    var images: [ImageModel]?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupViewModel()
        setupTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       setupActionButton()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.closeConnection()
        viewModel = nil
    }
    
    private func setupTableView()
    {
        tableView.register(UINib(nibName: NibIdentifier.kImageCell, bundle: nil), forCellReuseIdentifier: CellIdentifier.kImageCell)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupActionButton()
    {
        switch actionButtonState {
        case .connection:
            actionButton.setTitle("Connect", for: .normal)
            actionButton.isEnabled = true
            break
        case .send:
            actionButton.setTitle("Send", for: .normal)
            actionButton.isEnabled = true
            break
        }
    }
    
    private func setupViewModel()
    {
        viewModel = ConnectionViewModel()
        viewModel.delegate = self
    }
    
    private func validateTextField() -> Bool
    {
        if (ipAddressTextField.text?.isEmpty ?? true){
            
            return false
        }
        
        if (portNoTextField.text?.isEmpty ?? true)
        {
            return false
        }
        
        return true
        
    }
    
    @IBAction private func actionButtonTapped(_ sender: UIButton) {
        
        guard validateTextField() else {
            return
        }
        
        switch actionButtonState {
        case .connection:
            if validateTextField(), let portNo = Int(portNoTextField.text ?? ""){
                actionButton.isEnabled = false
                viewModel.connect(to: ipAddressTextField.text!, on: portNo)
            }
            break
        case .send:
            if let images = images{
                actionButton.isEnabled = false
                viewModel.send(images: images)
            }
            break
            
            
        }
    }
    
    
}

extension ConnectionViewController: ConnectionViewModelDelegate
{
    func showActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.isHidden = false
            self?.activityIndicator.startAnimating()
        }
        
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden = true
            self?.setupActionButton()
        }
        
        
    }
    
    func showAlert(with message: String) {
        
    }
    
    func updateConnectionStatus(with status: Bool) {
        if status {
            connectionStatusLabel.text = "Connected"
            actionButtonState = .send
        }
        else{
            connectionStatusLabel.text = "Not Connected"
            actionButtonState = .connection
        }
        setupActionButton()
    }
    
    func didGetNewImage() {
        tableView.reloadData()
    }
    
    
}

extension ConnectionViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.receivedImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.kImageCell, for: indexPath) as? ImageCell
        else{
            return UITableViewCell()
        }
        
        cell.customImageView.image = viewModel.receivedImages[indexPath.row]
        cell.customImageView.contentMode = .scaleAspectFit
        cell.imageDownloadProgressBar.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        250
    }
    
}
