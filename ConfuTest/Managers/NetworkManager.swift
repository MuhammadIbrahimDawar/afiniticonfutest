//
//  NetworkManager.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import Foundation

protocol NetworkManagerDelegate: class {
    func networkManager(_ networkManager: NetworkManager, didGet data: Data, with uuid:String)
    func networkManager(_ networkManager: NetworkManager, didFailWith Error: Error)
    func networkManager(_ networkManager: NetworkManager, didGet downloadProgress:Float, for uuid:String)
}

class NetworkManager: NSObject {
    
  
    private var threads = [DispatchQueue]()

    var noOfThreadRequired = 0{
        didSet{
            threads.removeAll()
            for _ in 1...noOfThreadRequired
            {
                threads.append(DispatchQueue.global(qos: .userInitiated))
            }
        }
    }
    var delegate: NetworkManagerDelegate?
    
    override init() {
        super.init()
    }
    
    
    func downloadData(from url: URL, with uuid: String, onthread: Int ) {
        
        threads[onthread - 1].async { [weak self] in
            self?.getData(from: url,with: uuid)
        }
        
    }
    
    private func getData(from url: URL, with uuid:String) {
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig,delegate: self, delegateQueue: nil)
        session.sessionDescription = uuid
        session.downloadTask(with: url).resume()
        
    }
    
    private func readDownloadedData(of url: URL) -> Data? {
        do {
            let reader = try FileHandle(forReadingFrom: url)
            let data = reader.readDataToEndOfFile()
                
            return data
        } catch {
            print(error)
            return nil
        }
    }
}

extension NetworkManager: URLSessionDownloadDelegate
{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        if let data = readDownloadedData(of: location){
            delegate?.networkManager(self, didGet: data, with: session.sessionDescription ?? "")
        }
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if totalBytesExpectedToWrite > 0 {
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            delegate?.networkManager(self, didGet: progress, for: session.sessionDescription ?? "" )
            
        }
    }
}
