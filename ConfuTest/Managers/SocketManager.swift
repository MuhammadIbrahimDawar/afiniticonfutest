//
//  SocketManager.swift
//  ConfuTest
//
//  Created by Muhammad Ibrahim on 02/06/2021.
//

import UIKit

protocol SocketDataManagerDelegate:class {
    func updateConnection(with status:Bool)
    func dataSent()
    func didGet(data: Data)
}

class SocketDataManager: NSObject {
    
    private var readStream: Unmanaged<CFReadStream>?
    private var writeStream: Unmanaged<CFWriteStream>?
    private var inputStream: InputStream?
    private var outputStream: OutputStream?
    private var messages = [AnyHashable]()
    
    var delegate: SocketDataManagerDelegate?
    var data: [Data]?
    
    override init(){
        super.init()
        
    }
    
    func connectWith(socket: ConnectionModel) {
        
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (socket.ipAddress as CFString), UInt32(socket.port), &readStream, &writeStream)
        messages = [AnyHashable]()
        open()
    }
    
    func disconnect(){
        
        close()
    }
    
    private func open() {
        print("Opening streams.")
        outputStream = writeStream?.takeRetainedValue()
        inputStream = readStream?.takeRetainedValue()
        outputStream?.delegate = self
        inputStream?.delegate = self
        outputStream?.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
        inputStream?.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
        outputStream?.open()
        inputStream?.open()
    }
    
    private func close() {
        print("Closing streams.")
        delegate?.updateConnection(with: false)
        inputStream?.close()
        outputStream?.close()
        inputStream?.remove(from: RunLoop.current, forMode: RunLoop.Mode.default)
        outputStream?.remove(from: RunLoop.current, forMode: RunLoop.Mode.default)
        inputStream?.delegate = nil
        outputStream?.delegate = nil
        inputStream = nil
        outputStream = nil
    }
    
    
    
    private func received(data: Data){
        
        delegate?.didGet(data: data)
    }
    
    func send()
    {
        guard data != nil, data!.count > 0  else {
            return
        }
        
        if let first = data?.first {
            let buff = [UInt8](first)
            
            outputStream?.write(buff, maxLength: buff.count)
        }
        data?.removeFirst()
        
        if data!.isEmpty{
            delegate?.dataSent()
        }
        
    }
}

// MARK:- Stream Delegate
extension SocketDataManager: StreamDelegate
{
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        print("stream event \(eventCode)")
        switch eventCode {
        case .openCompleted:
            delegate?.updateConnection(with: true)
            print("Stream opened")
        case .hasBytesAvailable:
            if aStream == inputStream {
                
                readDataFrom(stream: aStream as! InputStream)
            }
            break
        case .hasSpaceAvailable:
            print("Stream has space available now ")
            DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 1.5) { [weak self] in
                self?.send()
            }
            
        case .errorOccurred:
            print("\(aStream.streamError?.localizedDescription ?? "")")
        case .endEncountered:
            aStream.close()
            aStream.remove(from: RunLoop.current, forMode: RunLoop.Mode.default)
            print("close stream")
            delegate?.updateConnection(with: false)
        default:
            print("Unknown event")
        }
    }
    
    func readDataFrom(stream input: InputStream) {
        var data = Data()
        let bufferSize = 2024
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
        while input.hasBytesAvailable {
            let read = input.read(buffer, maxLength: bufferSize)
            data.append(buffer,count: read)
        }
        buffer.deallocate()
        
        received(data: data)
    }
}
